import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {
  registrationForm: FormGroup;

  constructor(
    private http: HttpClient,
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<RegisterComponent>
  ) {
    this.registrationForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      empId: ['', Validators.required],
      mobile: ['', Validators.required],
      makeManager:[false],
      dob: ['', Validators.required]
    });
  }

  onSubmit() {
    if (this.registrationForm.invalid) {
      return;
    }
    let headers=new HttpHeaders()
    .set("Authorization",`Bearer ${localStorage.getItem('token')}`)
    const newEmployee = this.registrationForm.value;
    console.log(newEmployee);
    if(newEmployee.makeManager){
      this.http.post('http://localhost:3000/auth/register-manager', newEmployee,{'headers':headers})
      .subscribe(
        response => {
          this.dialogRef.close(true);
          alert('Employee registered successfully');
        },
        error => {
          console.error('Failed to register employee', error);
        }
      );
    }
else{
  this.http.post('http://localhost:3000/auth/register-employee', newEmployee,{'headers':headers})
      .subscribe(
        response => {
          this.dialogRef.close(true);
          alert('Employee registered successfully');
        },
        error => {
          console.error('Failed to register employee', error);
        }
      );
}
    
  }
}
