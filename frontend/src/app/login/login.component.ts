import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  loginForm: FormGroup;
    roles={
    "employee":"6463ca238c4878be9edc1ade",
    "manager":"6463ca238c4878be9edc1add"
  }
  constructor(private formBuilder: FormBuilder, private http: HttpClient,public router:Router) { }


  ngOnInit() {
    this.loginForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password:new FormControl ('', [Validators.required, Validators.minLength(6)])
    });
  }

  get email() {
    return this.loginForm.get('email');
  }

  get password() {
    return this.loginForm.get('password');
  }

  onSubmit() {
    if (this.loginForm.invalid) {
      return;
    }

    const email = this.loginForm.value.email;
    const password = this.loginForm.value.password;

    // Create the request body
    const requestBody = { email, password };

    // Making the HTTP POST request
    this.http.post('http://localhost:3000/auth/login', requestBody)
      .subscribe(
        (response:any) => {
          
          
          localStorage.setItem("token",response.data.accessToken);
          console.log(response.data.role);
          localStorage.setItem('role',response.data.role);
          if(response.data.role===this.roles.manager){
            this.router.navigate(['/dashboard']);
            alert('Login successful');

          }

            else{
              alert("Unauthorized Access!!")
            }
        },
        error => {
        
          console.error('Login failed', error);
        }
      );
  }
}

