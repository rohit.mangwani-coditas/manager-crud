import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent {
  editForm: FormGroup;

  constructor(
    private http: HttpClient,
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<EditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.editForm = this.formBuilder.group({
      _id:[data.employee._id],
      empId: [data.employee.empId],
      name: [data.employee.name, Validators.required],
      email:[data.employee.email],
      dob: [data.employee.dob, Validators.required],
      mobile: [data.employee.mobile, Validators.required]
    });
  }

  onSubmit() {
    const confirmDelete = confirm('Are you sure you want to update this employee?');
    if (!confirmDelete) {
      return;
    }
    if (this.editForm.invalid) {
      return;
    }

    const empId = this.editForm.value._id
    const updatedEmployee = this.editForm.value;
   console.log(empId,updatedEmployee);
   let headers=new HttpHeaders()
    .set("Authorization",`Bearer ${localStorage.getItem('token')}`)
    this.http.patch(`http://localhost:3000/users/${empId}`, updatedEmployee,{'headers':headers})
      .subscribe(
        response => {
          this.dialogRef.close(updatedEmployee);
          alert('Employee updated successfully');
        },
        error => {
          console.error('Failed to update employee', error);
        }
      );
  }
}
