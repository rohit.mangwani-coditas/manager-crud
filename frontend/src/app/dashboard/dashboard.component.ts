import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { EditComponent } from '../edit/edit.component';
import { RegisterComponent } from '../register/register.component';
import { Router } from '@angular/router';

interface Employee {
  empId: string;
  name: string;
  dob: string;
  mobile: string;
}

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  employees = [];
  isEditModalOpen = false;
  editForm: FormGroup;
  roles={
    "employee":"6463ca238c4878be9edc1ade",
    "manager":"6463ca238c4878be9edc1add"
  }

  constructor(private http: HttpClient, 
    private formBuilder: FormBuilder,
    private dialog: MatDialog,
    private router:Router

    
    ) { }

  ngOnInit() {
  
    this.fetchEmployees();
    this.editForm = this.formBuilder.group({
      empId: [''],
      name: ['', Validators.required],
      dob: ['', Validators.required],
      mobile: ['', Validators.required]
    });
  }
  loggedRole=localStorage.getItem('role')
  fetchEmployees() {
    if(this.loggedRole!=this.roles.manager){
      this.router.navigate(['/login'])
    }
    let headers=new HttpHeaders()
    .set("Authorization",`Bearer ${localStorage.getItem('token')}`)
    
    console.log(localStorage)
    this.http.get<Employee[]>('http://localhost:3000/users/employee',{ 'headers': headers })
      .subscribe(
        employees => {
          
          this.employees = employees;
          console.log(employees)
        },
        error => {
          console.error('Failed to fetch employees', error);
        }
      );
  }

  deleteEmployee(empId: string) {
    const confirmDelete = confirm('Are you sure you want to delete this employee?');
    if (!confirmDelete) {
      return;
    }
    let headers=new HttpHeaders()
    .set("Authorization",`Bearer ${localStorage.getItem('token')}`)
    this.http.delete(`http://localhost:3000/users/${empId}`,{ 'headers': headers })
      .subscribe(
        response => {
          this.fetchEmployees();
          console.log('Employee deleted successfully');
        },
        error => {
          console.error('Failed to delete employee', error);
        }
      );
  }

  // openEditModal(employee: Employee) {
  //   this.editForm.patchValue({
  //     empId: employee.empId,
  //     name: employee.name,
  //     dob: employee.dob,
  //     mobile: employee.mobile
  //   });
  //   this.isEditModalOpen = true;
  // }
  openEditModal(employee: Employee) {
    const dialogRef = this.dialog.open(EditComponent, {
      width: '400px',
      data: { employee }
    });
  
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        // Perform any necessary actions after the dialog is closed
        console.log('Dialog was closed with result:', result);
        // Refresh the employee data
        this.fetchEmployees();
      }
    });
  }
  onSubmit() {
    if (this.editForm.invalid) {
      return;
    }

    const empId = this.editForm.value.empId;
    const updatedEmployee = this.editForm.value;
    let headers=new HttpHeaders()
    .set("Authorization",`Bearer ${localStorage.getItem('token')}`)
    this.http.patch(`http://localhost:3000/users/${empId}`, updatedEmployee,{ 'headers': headers })
      .subscribe(
        response => {
          this.fetchEmployees();
          this.isEditModalOpen = false;
          console.log('Employee updated successfully');
        },
        error => {
          console.error('Failed to update employee', error);
        }
      );
  }
  openRegistrationDialog() {
    const dialogRef = this.dialog.open(RegisterComponent, {
      width: '500px'
    });
  
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        // Perform any necessary actions after the dialog is closed
        console.log('Dialog was closed with result:', result);
        // Refresh the employee data or perform any other actions
        this.fetchEmployees();
      }
    });
  }

  viewManager(){
    this.router.navigate(['/managers'])
  }
}
