import { connect } from "mongoose";

export const connectToMongo=async()=>{
    try {
        const{MONGO_CONNECTION_URL}=process.env;
        await connect(MONGO_CONNECTION_URL||"");
        console.log("connected to Mongo");
        return true;
    } catch (error) {
        console.log(`could not connect too mongo `,error);
    }
    
}