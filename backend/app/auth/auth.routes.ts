import { Router } from "express";
import authService from "./auth.service";
import { ResponseHandler } from "../utility/response.handler";
import { roleValidator } from "../utility/middleware/role.validate";
import { roles } from "../utility/constants";
const router=Router();

router.post("/register-employee",roleValidator([roles.manager]),async(req,res,next)=>{
  try {
    const user=req.body;
    
    const result=await authService.registerEmployee(user);
   
    res.send(new ResponseHandler(result));
  
  } catch (error) {
    
        next(error);
  }
    
})
router.post("/register-manager",roleValidator([roles.manager]),async(req,res,next)=>{
    try {
      const user=req.body;
      const result=await authService.registerManager(user);
      res.send(new ResponseHandler(result));
    } catch (error) {
          next(error);
    }
      
  })
  router.post("/login",async(req,res,next)=>{
    try {   
        const userCredentials=req.body;
        const result=await authService.login(userCredentials);
        res.send(new ResponseHandler(result)); 
        
    } catch (error) {
        next(error);
    }
  })

export default router;