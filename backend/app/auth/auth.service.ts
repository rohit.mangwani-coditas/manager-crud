import userService from "../featured-modules/users/user.service"
import { IUser } from "../featured-modules/users/user.types"
import { roles } from "../utility/constants";
import { genSalt,compare,hash } from "bcryptjs";
import { ICredentials } from "./auth.types";
import { AUTH_RESPONSES } from "./auth.responses";
import { getPrivateKey } from "../utility/key.generate";
import { sign } from "jsonwebtoken";
const encrypt=async(user:IUser)=>{
    const salt=await genSalt(10);
    const hashedPassword=await hash(user.password,salt);
     user.password=hashedPassword;
     return user;

}
const registerManager=async(user:IUser)=>{
    await encrypt(user);
    const data={...user,role:roles.manager}
   const result= await userService.create(data);
   return result;
}
const registerEmployee=async(user:IUser)=>{
   
    await encrypt(user);
    const data={...user,role:roles.employee}
   const result= await userService.create(data);
   return result;
}


const login=async(user:ICredentials)=>{
    
    const isUser=await userService.findOne({email:user.email});
    if(!isUser) throw AUTH_RESPONSES.User_Not_Found;
    
   const isPasswordMatched=await compare(user.password,isUser?.password);
   if(!isPasswordMatched) throw AUTH_RESPONSES.Invalid_Password;
    const privateKey=getPrivateKey();
    const accessToken = sign({id: isUser, role: isUser.role}, privateKey, {algorithm: "RS256"});
    return {accessToken, role: isUser.role};

}
export default{
    registerEmployee,registerManager,login
}