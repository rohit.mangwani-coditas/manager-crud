import { FilterQuery, UpdateQuery } from "mongoose";
import { RoleModel } from "./role.schema";
import { IRole } from "./role.types";

const findOne=(filter:FilterQuery<IRole>)=>RoleModel.findOne(filter);
const create=(role:IRole)=>RoleModel.create(role);
const update = (filter: FilterQuery<IRole>, data: UpdateQuery<IRole>) => {
    return RoleModel.updateMany(filter, data);
}
const find = (filter: FilterQuery<IRole>) => RoleModel.find({isDeleted: false, ...filter});

export default{
    findOne,create,update,find
}
