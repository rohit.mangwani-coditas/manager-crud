// import { Router } from "express";
// import roleService from "./role.service";
// import { ResponseHandler } from "../../utility/response.handler";
// import { roleValidator } from "../../utility/middleware/role.validator";
// import { roles } from "../../utility/constants";

// const route=Router();

// route.post("/",roleValidator([roles.admin]),async(req,res,next)=>{
//     try {
//         const role=req.body;
//         const result=await roleService.create(role);
//         res.send(new ResponseHandler(result));

//     } catch (error) {
//         next(error)
//     }
// })
// route.get("/",roleValidator([roles.admin]),async(req,res,next)=>{
//     const result=await roleService.find({});
//     res.send(new ResponseHandler(result));
// })
// route.delete("/",roleValidator([roles.admin]),async(req,res,next)=>{
//     try {
//         const role=req.body;
//         const result=await roleService.remove(role);
//         res.send(new ResponseHandler(result));

//     } catch (error) {
//         next(error)
//     }
// })