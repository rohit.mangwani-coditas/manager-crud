import { FilterQuery, UpdateQuery } from "mongoose";
import { IRole } from "./role.types";
import roleRepo from "./role.repo";
import { ROLE_RESPONSE } from "./role.response";

const findOne=(filter:FilterQuery<IRole>)=>roleRepo.findOne(filter);
const create=async(role:IRole)=>{
    
    const ExistRole=await findOne({name:role.name})
    if(ExistRole) throw ROLE_RESPONSE.ROLE_ALREADY_EXIST
    await roleRepo.create(role);
}
const remove = async (filter: FilterQuery<IRole>) => {
    const result = await roleRepo.update(filter, { isDeleted: true });
    if (result.modifiedCount <= 0) throw ROLE_RESPONSE.UNABLE_TO_PROCEED;
    return ROLE_RESPONSE.DELETE_SUCESS;
}
const find = (filter: FilterQuery<IRole>) => roleRepo.find({isDeleted: false, ...filter});
export default{
    findOne,create,remove,find
}
