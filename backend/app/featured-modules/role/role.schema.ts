import { model } from "mongoose";
import { IRole } from "./role.types";
import { BaseSchema } from "../../utility/base.schema";

const RoleSchema = new BaseSchema({
    name: {
        type: String
    }
});
type RoleType = Document & IRole;
export const RoleModel = model<RoleType>("role", RoleSchema);

