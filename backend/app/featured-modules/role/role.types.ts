import { Schema } from "mongoose";

export interface IRole {
    _id?: Schema.Types.ObjectId;
    name: string;
}