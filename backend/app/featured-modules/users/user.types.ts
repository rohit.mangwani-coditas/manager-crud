import { Date, Schema } from "mongoose";

export interface IUser{
    id?:Schema.Types.ObjectId,
    empId:string,
    name:string,
    email:string,
    password:string,
    role?:string,
    address?:string,
    dob?:Date,
    mobile?:number
}