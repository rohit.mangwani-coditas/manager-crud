import { query } from "express-validator";
import { inputValidator } from "../../utility/middleware/input.validate";


export const Delete_USER_VALIDATION = [
   query("id").isNumeric().withMessage("Insert Id"),
    inputValidator
]
export const Update_USER_VALIDATION = [
    query("id").isNumeric().withMessage("Insert Id"),
     inputValidator
 ]
