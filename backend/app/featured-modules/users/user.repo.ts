import { FilterQuery, PipelineStage, UpdateQuery } from "mongoose";
import { UserModel } from "./user.schema";
import { IUser } from "./user.types";

 const create=(user:IUser)=>UserModel.create(user);
 const find=(filter:FilterQuery<IUser>)=>UserModel.find({isDeleted:false,...filter});
 const findOne=(user:FilterQuery<IUser>)=>UserModel.findOne({isDeleted:false,...user});
const update = (filter: FilterQuery<IUser>, data: UpdateQuery<IUser>) => UserModel.updateMany({ isDeleted: false, ...filter }, data);
const aggregation = (pipeline: PipelineStage[]) => UserModel.aggregate(pipeline);


export default{
    create,find,findOne,update,aggregation
}