import { FilterQuery, PipelineStage, UpdateQuery } from "mongoose";
import { IUser } from "./user.types";
import userRepo from "./user.repo";
import { USER_RESPONSE } from "./user.response";
import { roles } from "../../utility/constants";
import { sortAndPaginate } from "../../utility/sortAndPaginate";

 const create=async(user:IUser)=>{
    const ExistUser=await findOne({email:user.email});
    if(ExistUser) throw USER_RESPONSE.User_Alrready_Exists;
    const result=await userRepo.create(user);
    return result;

}
 const findAllEmployee=async()=>{
    
  const result=  await userRepo.find({role:roles.employee});
  return result;

}
const findAllManager=async()=>{
    
    const result=  await userRepo.find({role:roles.manager});
    return result;
  
  }
 const findOne=(user:FilterQuery<IUser>)=>userRepo.findOne(user);
const update = (filter: FilterQuery<IUser>, data: UpdateQuery<IUser>) => userRepo.update({isDeleted:false,...filter},data);
const aggregation = (pipeline: PipelineStage[]) => userRepo.aggregation(pipeline);
const deleteUser=async(userId:string)=>{
    const ExistUser=await findOne({_id:userId});
    if(!ExistUser) throw USER_RESPONSE.User_Does_Not_Exists;
    await update({_id:ExistUser.id},{isDeleted:true})

}
const find=(query:any)=>{
    const pipeline: PipelineStage[] = [];
    
    sortAndPaginate(query,pipeline);
    
    if(pipeline.length === 0) pipeline.push({$match: {}})
    return userRepo.aggregation(pipeline);
}
export default{
    create,findAllEmployee,findOne,update,
    aggregation,deleteUser,findAllManager,find
}


