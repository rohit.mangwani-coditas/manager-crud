import { Router } from "express";
import userService from "./user.service";
import { ResponseHandler } from "../../utility/response.handler";
import { roleValidator } from "../../utility/middleware/role.validate";
import { roles } from "../../utility/constants";

const router = Router();

router.get("/employee",roleValidator([roles.manager]), async (req, res, next) => {
    try {
        const result = await userService.findAllEmployee();
        res.send(new ResponseHandler(result));
    } catch (error) {
        next(error);
    }
})
// This is a get route that provides data in which you can dynamicaly filter thingss like name you can also sort 
//and use pagination 
//still working on how to present this in angular but works well in postman
router.get("/employee/filter",roleValidator([roles.manager]), async (req, res, next) => {
    try {
        const query=req.query;
        const result = await userService.find(query);
        
        res.send(new ResponseHandler(result));
    } catch (error) {
        next(error);
    }
})
router.get("/managers",roleValidator([roles.manager]), async (req, res, next) => {
    try {
        const result = await userService.findAllManager();
        res.send(new ResponseHandler(result));
    } catch (error) {
        next(error);
    }
})
router.patch("/:id",roleValidator([roles.manager]),async(req,res,next)=>{
    try {
            const userId=req.params.id;
            const updatedData=req.body;
            const{_id,...finalData}=updatedData;
            const result=await userService.update({_id:userId},{...finalData})
            res.send(new ResponseHandler(result));
    } catch (error) {
        
    }
})
router.delete("/:id",roleValidator([roles.manager]),async(req,res,next)=>{
    try {
            const userId=req.params.id;
            const result=await userService.deleteUser(userId);
            res.send(new ResponseHandler(result));
           
    } catch (error) {
        next(error)
    }
})

export default router;


