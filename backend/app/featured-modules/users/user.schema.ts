import { Document, Schema, model } from "mongoose";
import { BaseSchema } from "../../utility/base.schema";
import { IUser } from "./user.types";

const UserSchema=new BaseSchema({
    name:{
        type:String,
        required:true
    },
    email:{
        type:String,
        unique:true,
        
    },
    empId:{
     type:String,
     // required:true
    },
    role:{
        type:Schema.Types.ObjectId,
        ref:"role"
    },
    password:{
        type:String,
        required:true
    },
    address:{
        type:String,

    },
    dob:{
        type:Date,
        // required:true
    },
    mobile:{
        type:Number,

    }
})

export type UserDocument= Document & IUser;
export const UserModel=model<UserDocument>("user",UserSchema)