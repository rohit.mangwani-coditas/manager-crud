import { Route, Routes } from "./routes.types";
import AuthRouter from "../auth/auth.routes";
import UserRouter from "../featured-modules/users/user.routes"
import { ExcludedPath } from "../utility/middleware/token.validator";
export const routes:Routes=[
    new Route("/auth",AuthRouter),
    new Route("/users",UserRouter)
]

export const exculdedPath:ExcludedPath[]=[
    new ExcludedPath("/auth/login","GET")
]