import { Application, NextFunction, Request, Response, json } from "express";
import { exculdedPath, routes } from "./routes.data";
import { ResponseHandler } from "../utility/response.handler";
import cors from "cors";
import helmet from "helmet";
import { TokenValidator } from "../utility/middleware/token.validator";


export const RegisterRoutes=(app:Application)=>{
    app.use(helmet());
    app.use(cors())
    app.use(json());
    app.use(TokenValidator(exculdedPath))

    for(let route of routes){
        app.use(route.path,route.router);
    }

    app.use((error:any,req:Request,res:Response,next:NextFunction)=>{
       
        res.status(error.statusCode||500).send(new ResponseHandler(null,error));
    })
}