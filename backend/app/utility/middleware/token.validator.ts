import { NextFunction, Request, Response } from "express";
import { getPublicKey } from "../key.generate";
import { verify } from "jsonwebtoken";

export const TokenValidator=(exculdedPath:ExcludedPath[])=>{
     return (req:Request,res:Response,next:NextFunction)=>{
        try {
            if (exculdedPath.find(e => {
                if (req.url.includes(e.url)) {
                    if(e.excludeFromExcludePath.length === 0) return true;
                    else if(req.url.slice(e.url.length).startsWith("?")) return true;
                    const params = req.url.replace(e.url, "").split("/").slice(1);
                    for (let arr of e.excludeFromExcludePath) {
                        if (JSON.stringify(params) === JSON.stringify(arr)) {
                            return true;
                        }
                    }
                }
            })) {
                return next();
            }
    
            const token = req.headers.authorization?.split(" ")[1];
            if (!token) return next({ message: "Unauthorize", statusCode: 401 });
            
            const publicKey = getPublicKey();
            const payload = verify(token, publicKey || "");
            
            res.locals.payload = payload;
            next();
    
        } catch (error) {
            next(error)
        }
     } 
}


type  Method= "GET"|"POST"|"PATCH"|"DELETE"

export class ExcludedPath{
    excludeFromExcludePath: string[][] = [];
    constructor(public url:string,public method:Method,...excludeFromExcludePath:[][]){
        this.excludeFromExcludePath = excludeFromExcludePath;
    }
}