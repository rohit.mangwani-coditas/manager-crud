import { PipelineStage, Types, isValidObjectId } from "mongoose";


export const sortAndPaginate = (query: any, pipeline: PipelineStage[]) => {

    const {page, limit, sortBy, sortOrder, ...filterArr} = query;

    for(let filter in filterArr){
        if(isValidObjectId(filterArr[filter])) filterArr[filter] = new Types.ObjectId(filterArr[filter]);
        else if(parseInt(filterArr[filter]))filterArr[filter] = parseInt(filterArr[filter]);
        else if(filterArr[filter] === 'true' || filterArr[filter] === 'false'){
            filterArr[filter] = filterArr[filter] === 'true' ? true : false;
        }
        pipeline.push({
            $match: { isDeleted: false }
        });
        pipeline.push({
            $match: {
                [filter]: filterArr[filter]
            }
        })
    }

    const pageNo = page ? +page : 1;
    const limitNo = limit ? +limit : 3;
    const skipNo = (pageNo - 1) * limitNo;

    sortBy ?
    pipeline.push({
        $sort: {
            [sortBy]: sortOrder === 'desc' ? -1 : 1
        }
    }) : null;
    pipeline.push({
        $skip: skipNo
    });
    pipeline.push({
        $limit: limitNo
    });
}