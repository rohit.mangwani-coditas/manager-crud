import express from "express"
import { connectToMongo } from "./connections/mongo.connnection";
import { RegisterRoutes } from "./routes/routes";

export const StartServer=async()=>{
    const app=express();
    await connectToMongo();
    const{PORT}=process.env;
    RegisterRoutes(app);

    app.listen(PORT||3000,
            ()=>console.log(`The server started on ${PORT}`)
            )

}