"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RegisterRoutes = void 0;
const express_1 = require("express");
const routes_data_1 = require("./routes.data");
const response_handler_1 = require("../utility/response.handler");
const cors_1 = __importDefault(require("cors"));
const helmet_1 = __importDefault(require("helmet"));
const token_validator_1 = require("../utility/middleware/token.validator");
const RegisterRoutes = (app) => {
    app.use((0, helmet_1.default)());
    app.use((0, cors_1.default)());
    app.use((0, express_1.json)());
    app.use((0, token_validator_1.TokenValidator)(routes_data_1.exculdedPath));
    for (let route of routes_data_1.routes) {
        app.use(route.path, route.router);
    }
    app.use((error, req, res, next) => {
        res.status(error.statusCode || 500).send(new response_handler_1.ResponseHandler(null, error));
    });
};
exports.RegisterRoutes = RegisterRoutes;
