"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.exculdedPath = exports.routes = void 0;
const routes_types_1 = require("./routes.types");
const auth_routes_1 = __importDefault(require("../auth/auth.routes"));
const user_routes_1 = __importDefault(require("../featured-modules/users/user.routes"));
const token_validator_1 = require("../utility/middleware/token.validator");
exports.routes = [
    new routes_types_1.Route("/auth", auth_routes_1.default),
    new routes_types_1.Route("/users", user_routes_1.default)
];
exports.exculdedPath = [
    new token_validator_1.ExcludedPath("/auth/login", "GET")
];
