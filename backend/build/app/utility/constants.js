"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.populataDb = exports.roles = void 0;
const role_schema_1 = require("../featured-modules/role/role.schema");
exports.roles = {
    "employee": "6463ca238c4878be9edc1ade",
    "manager": "6463ca238c4878be9edc1add"
};
const populataDb = () => {
    role_schema_1.RoleModel.create([{ name: "employee" }, { name: "manager" }]);
};
exports.populataDb = populataDb;
