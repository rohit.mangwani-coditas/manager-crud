"use strict";
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.sortAndPaginate = void 0;
const mongoose_1 = require("mongoose");
const sortAndPaginate = (query, pipeline) => {
    const { page, limit, sortBy, sortOrder } = query, filterArr = __rest(query, ["page", "limit", "sortBy", "sortOrder"]);
    for (let filter in filterArr) {
        if ((0, mongoose_1.isValidObjectId)(filterArr[filter]))
            filterArr[filter] = new mongoose_1.Types.ObjectId(filterArr[filter]);
        else if (parseInt(filterArr[filter]))
            filterArr[filter] = parseInt(filterArr[filter]);
        else if (filterArr[filter] === 'true' || filterArr[filter] === 'false') {
            filterArr[filter] = filterArr[filter] === 'true' ? true : false;
        }
        pipeline.push({
            $match: { isDeleted: false }
        });
        pipeline.push({
            $match: {
                [filter]: filterArr[filter]
            }
        });
    }
    const pageNo = page ? +page : 1;
    const limitNo = limit ? +limit : 3;
    const skipNo = (pageNo - 1) * limitNo;
    sortBy ?
        pipeline.push({
            $sort: {
                [sortBy]: sortOrder === 'desc' ? -1 : 1
            }
        }) : null;
    pipeline.push({
        $skip: skipNo
    });
    pipeline.push({
        $limit: limitNo
    });
};
exports.sortAndPaginate = sortAndPaginate;
