"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const role_schema_1 = require("./role.schema");
const findOne = (filter) => role_schema_1.RoleModel.findOne(filter);
const create = (role) => role_schema_1.RoleModel.create(role);
const update = (filter, data) => {
    return role_schema_1.RoleModel.updateMany(filter, data);
};
const find = (filter) => role_schema_1.RoleModel.find(Object.assign({ isDeleted: false }, filter));
exports.default = {
    findOne, create, update, find
};
