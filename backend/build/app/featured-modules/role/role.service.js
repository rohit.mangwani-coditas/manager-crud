"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const role_repo_1 = __importDefault(require("./role.repo"));
const role_response_1 = require("./role.response");
const findOne = (filter) => role_repo_1.default.findOne(filter);
const create = (role) => __awaiter(void 0, void 0, void 0, function* () {
    const ExistRole = yield findOne({ name: role.name });
    if (ExistRole)
        throw role_response_1.ROLE_RESPONSE.ROLE_ALREADY_EXIST;
    yield role_repo_1.default.create(role);
});
const remove = (filter) => __awaiter(void 0, void 0, void 0, function* () {
    const result = yield role_repo_1.default.update(filter, { isDeleted: true });
    if (result.modifiedCount <= 0)
        throw role_response_1.ROLE_RESPONSE.UNABLE_TO_PROCEED;
    return role_response_1.ROLE_RESPONSE.DELETE_SUCESS;
});
const find = (filter) => role_repo_1.default.find(Object.assign({ isDeleted: false }, filter));
exports.default = {
    findOne, create, remove, find
};
