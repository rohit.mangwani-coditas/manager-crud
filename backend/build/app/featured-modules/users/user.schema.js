"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserModel = void 0;
const mongoose_1 = require("mongoose");
const base_schema_1 = require("../../utility/base.schema");
const UserSchema = new base_schema_1.BaseSchema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        unique: true,
    },
    empId: {
        type: String,
        // required:true
    },
    role: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: "role"
    },
    password: {
        type: String,
        required: true
    },
    address: {
        type: String,
    },
    dob: {
        type: Date,
        // required:true
    },
    mobile: {
        type: Number,
    }
});
exports.UserModel = (0, mongoose_1.model)("user", UserSchema);
