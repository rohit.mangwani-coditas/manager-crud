"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const user_schema_1 = require("./user.schema");
const create = (user) => user_schema_1.UserModel.create(user);
const find = (filter) => user_schema_1.UserModel.find(Object.assign({ isDeleted: false }, filter));
const findOne = (user) => user_schema_1.UserModel.findOne(Object.assign({ isDeleted: false }, user));
const update = (filter, data) => user_schema_1.UserModel.updateMany(Object.assign({ isDeleted: false }, filter), data);
const aggregation = (pipeline) => user_schema_1.UserModel.aggregate(pipeline);
exports.default = {
    create, find, findOne, update, aggregation
};
