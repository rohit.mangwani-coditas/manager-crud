"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const user_repo_1 = __importDefault(require("./user.repo"));
const user_response_1 = require("./user.response");
const constants_1 = require("../../utility/constants");
const sortAndPaginate_1 = require("../../utility/sortAndPaginate");
const create = (user) => __awaiter(void 0, void 0, void 0, function* () {
    const ExistUser = yield findOne({ email: user.email });
    if (ExistUser)
        throw user_response_1.USER_RESPONSE.User_Alrready_Exists;
    const result = yield user_repo_1.default.create(user);
    return result;
});
const findAllEmployee = () => __awaiter(void 0, void 0, void 0, function* () {
    const result = yield user_repo_1.default.find({ role: constants_1.roles.employee });
    return result;
});
const findAllManager = () => __awaiter(void 0, void 0, void 0, function* () {
    const result = yield user_repo_1.default.find({ role: constants_1.roles.manager });
    return result;
});
const findOne = (user) => user_repo_1.default.findOne(user);
const update = (filter, data) => user_repo_1.default.update(Object.assign({ isDeleted: false }, filter), data);
const aggregation = (pipeline) => user_repo_1.default.aggregation(pipeline);
const deleteUser = (userId) => __awaiter(void 0, void 0, void 0, function* () {
    const ExistUser = yield findOne({ _id: userId });
    if (!ExistUser)
        throw user_response_1.USER_RESPONSE.User_Does_Not_Exists;
    yield update({ _id: ExistUser.id }, { isDeleted: true });
});
const find = (query) => {
    const pipeline = [];
    (0, sortAndPaginate_1.sortAndPaginate)(query, pipeline);
    if (pipeline.length === 0)
        pipeline.push({ $match: {} });
    return user_repo_1.default.aggregation(pipeline);
};
exports.default = {
    create, findAllEmployee, findOne, update,
    aggregation, deleteUser, findAllManager, find
};
