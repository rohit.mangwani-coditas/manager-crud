"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Update_USER_VALIDATION = exports.Delete_USER_VALIDATION = void 0;
const express_validator_1 = require("express-validator");
const input_validate_1 = require("../../utility/middleware/input.validate");
exports.Delete_USER_VALIDATION = [
    (0, express_validator_1.query)("id").isNumeric().withMessage("Insert Id"),
    input_validate_1.inputValidator
];
exports.Update_USER_VALIDATION = [
    (0, express_validator_1.query)("id").isNumeric().withMessage("Insert Id"),
    input_validate_1.inputValidator
];
