"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const user_service_1 = __importDefault(require("../featured-modules/users/user.service"));
const constants_1 = require("../utility/constants");
const bcryptjs_1 = require("bcryptjs");
const auth_responses_1 = require("./auth.responses");
const key_generate_1 = require("../utility/key.generate");
const jsonwebtoken_1 = require("jsonwebtoken");
const encrypt = (user) => __awaiter(void 0, void 0, void 0, function* () {
    const salt = yield (0, bcryptjs_1.genSalt)(10);
    const hashedPassword = yield (0, bcryptjs_1.hash)(user.password, salt);
    user.password = hashedPassword;
    return user;
});
const registerManager = (user) => __awaiter(void 0, void 0, void 0, function* () {
    yield encrypt(user);
    const data = Object.assign(Object.assign({}, user), { role: constants_1.roles.manager });
    const result = yield user_service_1.default.create(data);
    return result;
});
const registerEmployee = (user) => __awaiter(void 0, void 0, void 0, function* () {
    yield encrypt(user);
    const data = Object.assign(Object.assign({}, user), { role: constants_1.roles.employee });
    const result = yield user_service_1.default.create(data);
    return result;
});
const login = (user) => __awaiter(void 0, void 0, void 0, function* () {
    const isUser = yield user_service_1.default.findOne({ email: user.email });
    if (!isUser)
        throw auth_responses_1.AUTH_RESPONSES.User_Not_Found;
    const isPasswordMatched = yield (0, bcryptjs_1.compare)(user.password, isUser === null || isUser === void 0 ? void 0 : isUser.password);
    if (!isPasswordMatched)
        throw auth_responses_1.AUTH_RESPONSES.Invalid_Password;
    const privateKey = (0, key_generate_1.getPrivateKey)();
    const accessToken = (0, jsonwebtoken_1.sign)({ id: isUser, role: isUser.role }, privateKey, { algorithm: "RS256" });
    return { accessToken, role: isUser.role };
});
exports.default = {
    registerEmployee, registerManager, login
};
