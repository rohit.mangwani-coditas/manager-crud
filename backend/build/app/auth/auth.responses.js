"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AUTH_RESPONSES = void 0;
exports.AUTH_RESPONSES = {
    User_Not_Found: {
        message: "user not found"
    },
    Invalid_Password: {
        message: "invalid password"
    }
};
