"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv_1 = require("dotenv");
const app_1 = require("./app/app");
const constants_1 = require("./app/utility/constants");
(0, dotenv_1.config)();
(0, app_1.StartServer)();
(0, constants_1.populataDb)();
